import { Link } from 'react-router-dom';
import { Layout, Breadcrumb, Table, Tag, Space, Button } from 'antd';
import './styles.css';
import Header from '../components/Header';
import Footer from '../components/Footer';
import { useEffect, useState } from 'react';
import { getOrdersList, Order } from '../services/orders.service';
import moment from 'moment-timezone';
import { useHistory } from 'react-router-dom';

const { Content } = Layout;

export const StatusColors = {
  created: '#3e3e3e',
  confirmed: '#87d068',
  cancelled: '#f50',
  delivered: '#108ee9',
};

function HomePage() {
  const history = useHistory();

  const [orders, setOrders] = useState<Order[]>([]);

  useEffect(() => {}, []);

  useEffect(() => {
    async function fetchOrders() {
      const response = await getOrdersList();
      if (response && response.orders) {
        setOrders(response.orders);
      }
    }

    const interval = setInterval(() => {
      fetchOrders();
    }, 5000);

    fetchOrders();
    return () => clearInterval(interval);
  }, []);

  function renderContent() {
    return <Table columns={columns} dataSource={orders} />;
  }

  return (
    <>
      <Layout className='layout'>
        <Header />
        <Content style={{ padding: '0 50px' }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>Orders</Breadcrumb.Item>
            <Breadcrumb.Item>List</Breadcrumb.Item>
          </Breadcrumb>
          <div className='site-layout-content'>
            {renderContent()}
            <Button
              type='primary'
              onClick={() => history.push(`/orders/create`)}
            >
              Create New Order
            </Button>
          </div>
        </Content>
        <Footer />
      </Layout>
      ,
    </>
  );
}

const columns = [
  {
    title: 'Order ID',
    dataIndex: 'id',
    key: 'id',
    render: (id: string) => {
      return <Link to={`/order/${id}`}>{id}</Link>;
    },
  },
  {
    title: 'Status',
    key: 'status',
    dataIndex: 'status',
    render: (status: string) => (
      <Tag color={StatusColors[status]}>{status.toUpperCase()}</Tag>
    ),
  },
  {
    title: 'Date',
    dataIndex: 'created_at',
    key: 'created_at',
    render: (created_at: string) => <>{moment(created_at).toString()}</>,
  },
  {
    title: 'Action',
    dataIndex: 'id',
    key: 'action',
    render: (id) => (
      <Space size='middle'>
        <Link to={`/order/${id}`}>View</Link>
      </Space>
    ),
  },
];

export default HomePage;
