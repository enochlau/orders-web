import { useEffect, useState } from 'react';
import { Layout, Breadcrumb, Form, Tag, Button } from 'antd';
import { useParams, Link } from 'react-router-dom';
import { Order, getOrder, cancelOrder } from '../services/orders.service';
import moment from 'moment-timezone';
import Header from '../components/Header';
import Footer from '../components/Footer';
import './styles.css';
import { StatusColors } from './Home';
import { useToasts } from 'react-toast-notifications';

const { Content } = Layout;

function OrderPage() {
  const params: { id: string } = useParams();
  const { addToast } = useToasts();

  const [order, setOrder] = useState<Order | null>(null);
  const [busy, setBusy] = useState<boolean>(false);
  const [form] = Form.useForm();

  async function fetchOrder(id: string) {
    setBusy(true);
    const response = await getOrder(id);
    setBusy(false);
    if (response && response.order) {
      setOrder(response.order);
    }
  }
  useEffect(() => {
    if (params.id) {
      fetchOrder(params.id);
    }
  }, [params.id]);

  async function handleCancelOrder() {
    const response = await cancelOrder(params.id);
    if (response) {
      addToast('Order cancelled', {
        appearance: 'success',
        autoDismiss: true,
      });
    }

    fetchOrder(params.id);
  }

  function renderContent() {
    if (order) {
      return (
        <Form
          labelCol={{ span: 4 }}
          wrapperCol={{ span: 14 }}
          layout={'horizontal'}
          form={form}
          initialValues={{ layout: 'horizontal' }}
          onValuesChange={() => {}}
        >
          <Form.Item label='Order ID'>
            <span className='order-id'>{params.id}</span>
          </Form.Item>
          <Form.Item label='Details'>{order?.data.details}</Form.Item>
          <Form.Item label='Status'>
            <Tag color={StatusColors[order?.status]}>
              {order?.status?.toUpperCase()}
            </Tag>
          </Form.Item>
          <Form.Item label='Created On'>
            {moment(order?.created_at).toString()}
          </Form.Item>
          <Form.Item label='Last Modified'>
            {moment(order?.modified_at).toString()}
          </Form.Item>
          <Form.Item wrapperCol={{ span: 14, offset: 4 }}>
            {order?.status === 'confirmed' && (
              <Button
                type='primary'
                disabled={busy}
                id='cancel'
                onClick={handleCancelOrder}
              >
                {busy ? 'Cancelling...' : 'Cancel Order'}
              </Button>
            )}
          </Form.Item>
        </Form>
      );
    } else {
      return <div>Sorry, that order does not exist.</div>;
    }
  }

  return (
    <>
      <Layout className='layout'>
        <Header />
        <Content style={{ padding: '0 50px' }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>
              <Link to='/'>Orders</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>View</Breadcrumb.Item>
            <Breadcrumb.Item>{params.id}</Breadcrumb.Item>
          </Breadcrumb>
          <div className='site-layout-content'>{renderContent()}</div>
        </Content>
        <Footer />
      </Layout>
      ,
    </>
  );
}

export default OrderPage;
