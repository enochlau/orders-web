import { useState } from 'react';
import { Layout, Menu, Breadcrumb, Form, Input, Button, Radio } from 'antd';
import { Link } from 'react-router-dom';
import './styles.css';
import { submitOrder, SubmitOrderResponse } from '../services/orders.service';
import { useHistory } from 'react-router-dom';
import { useToasts } from 'react-toast-notifications';
import Header from '../components/Header';
import Footer from '../components/Footer';

const { Content } = Layout;

function OrderPage() {
  const history = useHistory();
  const { addToast } = useToasts();

  const [busy, setBusy] = useState<boolean>(false);
  const [details, setDetails] = useState<string>('');

  async function handleSubmit() {
    setBusy(true);
    if (!details || details.length === 0) {
      addToast('Please enter the order details', {
        appearance: 'error',
        autoDismiss: true,
      });
      setBusy(false);
      return;
    }

    const response: SubmitOrderResponse = await submitOrder({ details });
    setBusy(false);
    if (!response) {
      addToast('There was an error.', {
        appearance: 'error',
        autoDismiss: true,
      });
    }
    if (response.errorMessage) {
      addToast(response.errorMessage, {
        appearance: 'error',
        autoDismiss: true,
      });
    } else if (response.order) {
      addToast(`Order ${response.order.id} created`, {
        appearance: 'success',
        autoDismiss: true,
      });
      history.push('/');
    }
  }

  function renderContent() {
    return (
      <Form
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 14 }}
        layout={'horizontal'}
        initialValues={{ layout: 'horizontal' }}
        onValuesChange={() => {}}
      >
        <Form.Item label='Order Details'>
          <Input
            placeholder='Additional details about the order'
            onChange={(event) => setDetails(event.target.value)}
            disabled={busy}
            autoFocus={true}
          />
        </Form.Item>
        <Form.Item wrapperCol={{ span: 14, offset: 4 }}>
          <Button disabled={busy} type='primary' onClick={handleSubmit}>
            {busy ? 'Submitting...' : 'Submit Order'}
          </Button>
        </Form.Item>
      </Form>
    );
  }

  return (
    <>
      <Layout className='layout'>
        <Header />
        <Content style={{ padding: '0 50px' }}>
          <Breadcrumb style={{ margin: '16px 0' }}>
            <Breadcrumb.Item>
              <Link to='/'>Orders</Link>
            </Breadcrumb.Item>
            <Breadcrumb.Item>Create New Order</Breadcrumb.Item>
          </Breadcrumb>
          <div className='site-layout-content'>{renderContent()}</div>
        </Content>
        <Footer />
      </Layout>
      ,
    </>
  );
}

export default OrderPage;
