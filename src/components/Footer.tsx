import { Layout } from 'antd';
export default function Footer() {
  return (
    <Layout.Footer style={{ textAlign: 'center' }}>
      Usada Kensetsu Fictional Corporation ©2021
    </Layout.Footer>
  );
}
