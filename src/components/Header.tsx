import { Layout } from 'antd';
export default function Header() {
  return (
    <Layout.Header>
      <div className='logo' />
      <h1 color='white'>Orders Application</h1>
    </Layout.Header>
  );
}
