/* eslint-disable react/prop-types */
// @ts-nocheck
import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom';
import HomePage from './pages/Home';
import OrderCreate from './pages/OrderCreate';
import OrderView from './pages/OrderView';

export default function Router() {
  const routes = [
    {
      path: '/',
      component: HomePage,
    },
    {
      path: '/orders/create',
      component: OrderCreate,
    },
    {
      path: '/order/:id',
      component: OrderView,
    },
  ];

  return (
    <BrowserRouter>
      <Switch>
        {routes.map((route, i) => {
          return (
            <Route
              key={i}
              exact
              path={route.path}
              component={route.component}
            />
          );
        })}
      </Switch>
    </BrowserRouter>
  );
}
