import React from 'react';
import ReactDOM from 'react-dom';
import 'antd/dist/antd.css';
import Router from './Router';
import reportWebVitals from './reportWebVitals';
import { ToastProvider } from 'react-toast-notifications';

ReactDOM.render(
  <ToastProvider>
    <Router />
  </ToastProvider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
