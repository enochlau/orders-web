import fetch from 'node-fetch';
const baseUrl =
  process.env.REACT_APP_BASE_URL ||
  'http://localhost:4000/local'

export const get = async <T>(requestPath: string) => {
  const actionUrl = `${baseUrl}${requestPath}`;
  let response;
  try {
    response = await fetch(actionUrl, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response.json();
  } catch (error) {
    console.error('API Service error', error);
    return { error };
  }
};

export const post = async <T>(requestPath: string, body: { [key: string]: string }) => {
  const actionUrl = `${baseUrl}${requestPath}`;
  let response;
  try {
    response = await fetch(actionUrl, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    });
    return response.json() as T;
  } catch (error) {
    console.error('API Service error', error);
    return { error };
  }
};

export const put = async<T>(requestPath: string, body: { [key: string]: string }) => {
  const actionUrl = `${baseUrl}${requestPath}`;
  let response;
  try {
    response = await fetch(actionUrl, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(body)
    });
    return response.json();
  } catch (error) {
    console.error('API Service error', error);
    return { error };
  }
};

export const del = async <T>(requestPath: string) => {
  const actionUrl = `${baseUrl}${requestPath}`;
  let response;
  try {
    response = await fetch(actionUrl, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
      },
    });
    return response.json();
  } catch (error) {
    console.error('API Service error', error);
    return { error };
  }
};
