import * as ApiService from './api.service';

export interface Order {
  id: string;
  data: OrderDetails;
  status: 'created' | 'cancelled' | 'confirmed' | 'delivered';
  created_at: string;
  modified_at: string;
  deleted_at: string;
}

interface OrderDetails {
  details: string;
}

export interface SubmitOrderResponse {
  order?: Order
  errorMessage?: any
}

const ordersPath = '/orders';

function getOrder(id: string) {
  const actionPath = `${ordersPath}/${id}`;
  return ApiService.get(actionPath);
}

function getOrdersList() {
  const actionPath = `${ordersPath}`;
  return ApiService.get(actionPath);
}

function submitOrder(body: { [key: string]: string }) {
  const actionPath = `${ordersPath}`;
  return ApiService.post(actionPath, body);
}

function cancelOrder(id: string) {
  const actionPath = `${ordersPath}/${id}`;
  return ApiService.put(actionPath, { status: 'cancelled' });
}

export { getOrder, getOrdersList, submitOrder, cancelOrder };
